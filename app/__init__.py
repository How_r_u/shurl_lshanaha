import aioredis
import docker
import os

from redis import Redis
from decouple import config
from fastapi_jwt_auth import AuthJWT
from pydantic import BaseModel


try:
    DEBUG = int(os.environ.get('DEBUG', 1))
except Exception:
    DEBUG = False


def get_docker_container_ip(container_name):
    client = docker.DockerClient()
    container = client.containers.get(container_name)
    ip_add = container.attrs['NetworkSettings']['Networks']['shurl_default']['IPAddress']
    return ip_add


async def get_async_redis():
    try:
        pool = await aioredis.create_redis_pool(
            ('localhost', 6379), encoding='utf-8',
            password=config('REDIS_PASSWORD'))
        return pool
    except ConnectionRefusedError as e:
        raise Exception(e)


sync_redis = Redis(host='localhost', port=6379, db=0,
                   decode_responses=True, encoding='utf-8', password=config('REDIS_PASSWORD'))


# настройка jwt-auth, все подхватывается приложением
class Settings(BaseModel):
    authjwt_secret_key: str = config('secret')
    authjwt_denylist_enabled: bool = bool(config('denylist'))
    authjwt_denylist_token_checks: set = {"access", "refresh"}


# callback to get your configuration
@AuthJWT.load_config
def get_config():
    return Settings()


@AuthJWT.token_in_denylist_loader
def check_if_token_in_denylist(decrypted_token):
    jti = decrypted_token.get('jti')
    if not jti:
        return True
    in_denylist = sync_redis.hget('token', jti)
    return in_denylist == 'true'
