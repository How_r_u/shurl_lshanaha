from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from decouple import config
from app import get_docker_container_ip


def get_product_settings():
    engine = create_engine(f'postgresql://{config("POSTGRES_USER")}:{config("POSTGRES_PASSWORD")}'
                           f'@pg_comradz2:5432/Comradz', echo=False)
    SessionLocal = sessionmaker(bind=engine)
    return SessionLocal


Base = declarative_base()


def get_db():
    SessionLocal = get_product_settings()
    db = SessionLocal()
    try:
        yield db
    except Exception as e:
        print(e)
        db.close()


# alembic revision --autogenerate -m "Added account table" --head head
# alembic upgrade head
