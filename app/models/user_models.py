from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey, CheckConstraint
from sqlalchemy.sql import func, expression
from sqlalchemy.orm import relationship

from app.models import Base


class User(Base):

    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String, unique=True, nullable=False)
    password = Column(String, nullable=False)
    email = Column(String, unique=True, nullable=False)
    creation_time = Column(DateTime(timezone=True), server_default=func.now())
    validated = Column(Boolean, server_default=expression.false(), nullable=False)
    validate_time = Column(DateTime(timezone=True), onupdate=func.now())
    active = Column(Boolean, server_default=expression.false(), nullable=False)

    permission_id = Column(Integer, ForeignKey('permissions.id'),
                           nullable=False, default=1)

    permission = relationship('Permissions', back_populates="user", uselist=False, cascade='delete')

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = password

    def __repr__(self):
        return f"id: {self.id or None}, username: {self.username}," \
               f" email: {self.email}, password: {self.password}"


class Permissions(Base):
    __tablename__ = 'permissions'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)

    user = relationship('User', back_populates='permission')
