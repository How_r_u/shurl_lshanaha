import uvicorn
from app import DEBUG
from app.custom_logging import initialize_logs

if __name__ == '__main__':
    uvicorn.run("app.main:web_app",
                host='0.0.0.0',
                port=8888,
                reload=bool(DEBUG),
                debug=bool(DEBUG),
                log_config=initialize_logs()
                )
